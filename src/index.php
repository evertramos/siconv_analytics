<?php

echo 'Teste de API - SICONV [file_get_contents]';
echo '<br>';
echo '<hr>';
echo '<br>';

//$base_url = 'http://api.convenios.gov.br/siconv/v1/consulta.json';
//$base_url = 'http://api.convenios.gov.br/siconv/v1/consulta.html';
//$base_url = 'http://api.convenios.gov.br/siconv/v1/consulta/convenios.html';
//$base_url = 'http://api.convenios.gov.br/siconv/v1/consulta/convenios.json';
//$base_url = 'http://api.convenios.gov.br/siconv/v1/consulta/situacoes_publicacao_convenio.json';
$base_url = 'http://api.convenios.gov.br/siconv/v1/consulta/orgaos.json?nome=turismo';


$data = file_get_contents($base_url);

print_r($data);

echo '<br>';
echo '<hr>';
echo '<hr>';
echo '<br>';

echo 'Teste de API - SICONV - [Curl]';
echo '<br>';
echo '<hr>';
echo '<br>';

//$url = 'http://api.convenios.gov.br/siconv/v1/consulta/situacoes_publicacao_convenio.json';
$url = 'http://api.convenios.gov.br/siconv/v1/consulta/orgaos.json';

$curl = curl_init();

curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_URL, $url);

$result = curl_exec($curl);

curl_close($curl);

var_dump(json_decode($result, true));

echo '<br>';
echo '<hr>';
echo '<hr>';
echo '<br>';
echo 'Resultado: A API nãoe está respondendo conforme projetada. Nem mesmo os exemplos em (<a href="http://api.convenios.gov.br/siconv/doc/" target="_blank">http://api.convenios.gov.br/siconv/doc/</a>) estão funcionando corretamente.';
echo '<br>';
echo '<hr>';
echo '<hr>';
echo '<br>';
